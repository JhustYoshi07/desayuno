﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Desayuno
{
    class Program
    {

        //Jhustin Ismael Arias Perez 4-A T/M
   
            static async Task Main(string[] args)
            {
                Cafe Taza = Hacer_Cafe();
                Console.WriteLine("El cafe esta listo");
                
                //Variables de los alimentos

                var Task_Huevos = Freir_Huevos_Async(2);
                var Task_Tocino = Freir_Tocino_Async(3);
                var Task_Tostada = Echarle_Mantequilla_Async(2);

                //Proceso de la variable desayuno task

                var Task_Desayuno = new List<Task> { Task_Huevos, Task_Tocino, Task_Tostada };

                while (Task_Desayuno.Count > 0)
                {
                    Task Terminado = await Task.WhenAny(Task_Desayuno);
                    if (Terminado == Task_Huevos)
                    {
                        Console.WriteLine("Los huevos estan listos");
                    }
                    else if (Terminado == Task_Tocino)
                    {
                        Console.WriteLine("El tocino esta listo");
                    }
                    else if (Terminado == Task_Tostada)
                    {
                        Console.WriteLine("El pan tostado esta listo");
                    }
                    Task_Desayuno.Remove(Terminado);
                }

                Jugo juguito = Hacer_Jugo();
               
                Console.WriteLine("¡El desayuno esta listo!");
            }

            static async Task<Toast> Echarle_Mantequilla_Async(int numero)
            {
                var Pan_Tostado = await Pan_Tostado_Async(numero);
                Poner_Mantequilla(Pan_Tostado);
                Poner_Mermelada(Pan_Tostado);

                return Pan_Tostado;
            }

            private static Jugo Hacer_Jugo()
            {
                Console.WriteLine("Se esta preparando el jugo");
                return new Jugo();
            }

            //Procedimiento del pan tostado

            private static void Poner_Mermelada(Toast Pan_Tostado) =>
                Console.WriteLine("Poniendo mermelada al pan");

            private static void Poner_Mantequilla(Toast Pan_Tostado) =>
                Console.WriteLine("Poniendo mantequilla al pan");

            private static async Task<Toast> Pan_Tostado_Async(int rebanadas)
            {
                for (int rebanada = 0; rebanada < rebanadas; rebanada++)
                {
                    Console.WriteLine("El pan esta en el tostador");
                }
                Console.WriteLine("Pan tostandose....");
                await Task.Delay(3000);
                Console.WriteLine("Sacando del tostador...");

                return new Toast();
            }

            private static async Task<Bacon> Freir_Tocino(int rebanadas)
            {
                Console.WriteLine($"poniendo {rebanadas} rebanadas de tocino al pan");
                Console.WriteLine("Cocinando una rebanada de tocino...");
                await Task.Delay(3000);
                for (int rebanada = 0; rebanada < rebanadas; rebanada++)
                {
                    Console.WriteLine("volteando tocino...");
                }

                return new Bacon();
            }

            private static async Task<Egg> Freir_Huevos(int cantidad)
            {
                Console.WriteLine("Calentando los huevos");
                await Task.Delay(3000);
                Console.WriteLine($"Hierviendo {cantidad} huevos");
                Console.WriteLine("Cocinando los huevos ...");
                await Task.Delay(3000);
                Console.WriteLine("Poniendo los huevos en el plato...");

                return new Egg();
            }

            private static Cafe Hacer_Cafe()
            {
                Console.WriteLine("Sirviendo cafe...");
                return new Cafe();
      
        
        
        }
        }
    }


